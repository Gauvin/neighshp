
require(R6)







#' Montreal specific factory method for neighShp creation
#'
#' Implements
#' 1-createNeighShp: returns a neighShpQc instance
#'
#'
neighShpQcFactory <- R6Class(


  #
  #Name
  #
  "neighShpQcFactory",

  #--
  #Public
  #---


  public = list(

    #' Constructor
    #'
    #' @param rasterPath
    #' @param neighPath
    initialize = function(  neighPath=here::here("Data","GeoData","QuebecNeighbourhoods") ){



      private$neighPath <- neighPath





    },

    createNeighShp= function(listNeigh=NULL){

      myNeighShpQc <- neighShp::neighShpQc$new( neighPath=private$neighPath,
                                        listNeigh=listNeigh)

      return(myNeighShpQc)
    }

  ),


  private = list( neighPath=NULL )

)


